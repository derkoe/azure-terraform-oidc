# Azure Federated Token Test

This shows the integration of [Azure workload identiry federation](https://docs.microsoft.com/en-us/azure/active-directory/develop/workload-identity-federation) with Terraform and GitLab CI.

## Howto

1. Create a service principal (you can also use an existing one).

    ```sh
    az ad sp create-for-rbac -n "my-service-principal"
    ```

    Then assign the service principal permissions in your Azure subscription or resource group (via "Access control (IAM)")

2. Go to https://portal.azure.com/#view/Microsoft_AAD_IAM/ActiveDirectoryMenuBlade/~/RegisteredApps where you you'll find the service principal as "App registration"

3. Open "Certificates & secrets" and then the tab "Federated credentials" add a new credential with the following data

   * Issuer: https://gitlab.com (or your custom GitLab installation)
   * Subject Identifier: the GitLab reference, e.g.: `project_path:derkoe/azure-terraform-oidc:ref_type:branch:ref:main`
   * Name: a descriptive name for the federated credental (e.g. derkoe-azure-terraform-oidc-main)
   * Description: Optional, a description for the federated credential.
   * Audience: https://gitlab.com

4. Add the following variables to Settings - CI/CD:

    * ARM_CLIENT_ID - the client ID of your service principal
    * ARM_SUBSCRIPTION_ID - the id of the Azure subscription you want to deploy to
    * ARM_TENANT_ID - your AAD tenant id
    * TF_STATE_NAME - the state name you would like to use - e.g. "prod"

## References

- https://stackoverflow.com/questions/71285687/how-do-i-authenticate-using-a-managed-identity-from-gitlab-ci-to-push-a-docker-c
- https://blog.alexellis.io/deploy-without-credentials-using-oidc-and-github-actions/
